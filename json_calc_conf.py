import json
import os
from math import sqrt
import csv

def csv_write(FileName, mean_1, standard_deviation, accuracy, Word):
    # file = 'C:\\Users\\mohamedabrar.a\\Desktop\\Json_calc\\Humana0001_with_words.csv'
    # # print(FileName)
    # file_exists = os.path.isfile(file)
    # with open(file, 'a', newline='', encoding='utf-8') as csvfile:
    #     fieldnames = ['Name', 'Mean', "standard Deviation", "Accuracy", "Word"]
    #     writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=',', lineterminator='\n')
    #     if not file_exists:
    #         writer.writeheader()
    #     writer.writerow({'Name': FileName, 'Mean': mean_1, "standard Deviation": standard_deviation, "Accuracy": accuracy, "Word": Word})
        print(Word)

def parse_json(json_path, filename):
    with open(json_path) as s:
        jsondata = json.load(s)
        convert_to_dict = dict(jsondata)
        numoflines = len(jsondata)
        total = []
        word_list = []
        for line_num, next_char in convert_to_dict["Lines"].items():
            for main_keys, main_items in sorted(next_char.items()):
                confidence = main_items["conf"]
                Word = main_items["word"]
                word_list.append(Word)
                # print(confidence, "aaa", Word)
                if confidence != -1:
                    conf = int(confidence)
                    total.append(conf)
        length = len(total)
        mean = sum(total)

        mean_1 = mean / length
        differences = [x - mean_1 for x in total]
        sq_differences = [d ** 2 for d in differences]
        ssd = sum(sq_differences)
        ssd_1 = ssd / length
        standard_deviation = sqrt(ssd_1)

        meanX = 0.063898
        stdDevX = 0.029011
        InterceptX = 5.17892
        predicted_accuracy_1 = ((mean_1 * meanX) + (standard_deviation * stdDevX)) - InterceptX
        accuracy = predicted_accuracy_1 * 100
        # print("Mean: {}".format(mean_1))
        # print("standard deviation: {}".format(standard_deviation))
        # print("Accuracy: {}".format(accuracy))
        FileName = filename.replace(".json", "")
        for word in word_list:
            csv_write(FileName, mean_1, standard_deviation, accuracy, word)


SourcePath = "C:\\Users\\mohamedabrar.a\\Desktop\\OCR\\json's\\1"

for root, dirs, files in os.walk(SourcePath):
    for f in files:
        if f.endswith(".json"):
            path = root + "\\" + f
            parse_json(path, f)

